1. Open terminal in current folder.
2. Run this command to start the game "ruby task2.rb".
3. All instructions about game, you will see in console.

You can use this commands:
info - to see information about your pet
play - play with your pet in house
scratch - to scratch your pet
walk - walk with your pet outdoors
eat - to eat something
pills - give some pills for your pet
sleep - go to sleep
watch - watching for your pet
clean - clean your pet
help - to see the list of available commands
end - finish the game
