array = [621, 445, 147, 159, 430, 222, 482, 44, 194, 522, 652, 494, 14, 126, 532, 387, 441, 471, 337, 446, 18, 36, 202, 574, 556, 458, 16, 139, 222, 220, 107, 82, 264, 366, 501, 319, 314, 430, 55, 336]
puts "Array: " + array.to_s

puts "Amount of elements: " + array.size.to_s
puts "Reverse: " + array.reverse.to_s
puts "Maximum element: " + array.max.to_s
puts "Minimum element: " + array.min.to_s
puts "Sort from smallest to largest: " + array.sort.to_s
puts "Sort from largest to smallest: " + array.sort { |a, b| b <=> a }.to_s
puts "Without odd elements: " + array.select { |e| e.even? }.to_s
puts "Without remainder are divided by 3: " + array.select { |e| e % 3 == 0 }.to_s
puts "Without repetition: " + array.uniq.to_s
puts "Divide each element by 10: " + array.map { |e| e / 10.0 }.to_s

letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
puts "Letters of the English alphabet: " + array.select { |e| e.to_i < letters.size }.map { |e| letters[e.to_i] }.to_s

swap_array = array
min_index = array.index(array.min)
max_index = array.index(array.max)
swap_array[min_index], swap_array[max_index] = swap_array[max_index], swap_array[min_index]
puts "Swap the minimum and maximum elements: " + swap_array.to_s

puts "Elements before the minimum number: " + array[0...array.index(array.min)].to_s

new_array = array
puts "Three smallest elements: " + new_array.delete(new_array.min).to_s + ", " + new_array.delete(new_array.min).to_s + ", " + new_array.delete(new_array.min).to_s
