require 'html_gem'
require_relative 'pet'

def create_html_file(pet)
  head = "<head>
<title>Tamagotchi</title>
<link rel='stylesheet' type='text/css' href='application.css'>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
</head>"

  body = "<body>
<div class='header'>
  <p>Hello, #{pet.owner_name}!</p>
  <p>I'm a #{pet.type} and my name is #{pet.name}</p>
</div>

<div class='info-block'>
  <img class='image' src='#{pet.image}' alt='pet image'>
  <div class='message'>#{pet.message}</div>
  <div class='info'>
    <div class='info-item'>Food: #{pet.food}</div>
    <div class='info-item'>Cleanliness: #{pet.cleanliness}</div>
    <div class='info-item'>Mood: #{pet.mood}</div>
    <div class='info-item'>Health: #{pet.health}</div>
    <div class='info-item'>Sleep: #{pet.sleep}</div>
    <div class='info-item'>Life: #{pet.life}</div>
  </div>
</div>
</body>"

  html = "<!DOCTYPE html>
<html>
#{head}
#{body}
</html>"

  HTMLGem.create_html_file(html)
  HTMLGem.create_html_file(html, 'without-html')
  HTMLGem.create_html_file(html, 'with-html', true)
end

types = ["dog", "cat", "bird", "raccoon"]
%w[dog cat bi]
%i[new create] == [:new, :create]
pet_type = types.sample
puts "Hello, I'm your #{pet_type}. Please, give me a name: "
pet_name = gets.chomp
pet = Pet.new pet_name.to_s.capitalize, pet_type

puts "What is your name?"
owner_name = gets.chomp
pet.owner_name = owner_name.to_s
puts "Nice to meet you, " + owner_name

puts "Let's play!\n\n"
puts pet.help

create_html_file(pet)
game_continue = true
while game_continue
  game_continue = pet.game_continue
  if !game_continue
    puts "GAME OVER!"
    break
  end

  command = gets.chomp
  case command.strip.downcase
  when "info"
    pet.info
  when "sleep"
    pet.go_sleep
  when "play"
    pet.play_games
  when "scratch"
    pet.scratch
  when "walk"
    pet.go_walk
  when "pills"
    pet.give_pills
  when "clean"
    pet.clean
  when "eat"
    pet.give_food
  when "watch"
    pet.watch
  when "help"
    pet.help
  when "end"
    game_continue = false
    puts "Bye, #{pet.owner_name}!"
  else
    puts "I don't understand"
  end

  create_html_file(pet)
end
