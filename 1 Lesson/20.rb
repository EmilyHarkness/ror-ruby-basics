# Дан целочисленный массив. Найти минимальный из его локальных минимумов.

array = [-2, 5, -7, 8, 10, -20, 12]
new_array = []

array.each_cons(3) { |prev, curr, nxt|
    if prev > curr && curr < nxt
        new_array.push(curr)
    end
}

# Для крайніх елементів
if array.size > 2
    if  array[0] < array[1]
        new_array.push(array[0])
    end

    if array[-2] > array[-1]
        new_array.push(array[-1])
    end
elsif array.size == 2
    if  array[0] < array[1]
        new_array.push(array[0])
    else
        new_array.push(array[1])
    end
end

puts new_array.min