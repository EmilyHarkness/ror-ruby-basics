# Дано вещественное число R и массив вещественных чисел. Найти элемент массива, который наименее близок к данному числу.

array = [3.4, 2.4, 2.5, 7.8, -5.1, 4.9]
r = 5.2

max_difference = array.first > r ? array.first - r : r - array.first
result_element = array.first

array.each { |e|
    value = e > r ? e - r : r - e
    if value > max_difference
        max_difference = value
        result_element = e
    end
}

puts result_element