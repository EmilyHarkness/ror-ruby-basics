# Дан целочисленный массив. Найти количество максимальных элементов.

array = [-20, 5, -7, 8, 12, 10, -20, 12]
max_element = array.max
counter = 0

array.each{ |e|
    if e == max_element
        counter += 1
    end
}

puts counter