# Дан целочисленный массив. Осуществить циклический сдвиг элементов массива влево на одну позицию.

array = [-2, 5, -7, 8, 10, -20]
first_element = array.first

array = array.map.with_index { |e, i| 
    if i < array.size - 1
        e = array[i + 1]
    else
        first_element
    end
}
puts array.to_s