# Дан целочисленный массив. Найти количество минимальных элементов.

array = [-20, 5, -7, 8, 12, 10, -20, 12]
min_element = array.min
counter = 0

array.each{ |e| 
    if e == min_element
        counter += 1
    end
}

puts counter