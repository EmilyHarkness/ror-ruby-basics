# Дан целочисленный массив. Найти количество его локальных максимумов.

array = [-2, 5, -7, 8, 10, -20, 12]
counter = 0

array.each_cons(3) { |prev, curr, nxt|
    if prev < curr && curr > nxt
        counter += 1
    end
}

if array.size > 2 # для крайніх елементів
    if  array[0] > array[1]
        counter += 1
    end

    if array[-2] < array[-1]
        counter += 1
    end
elsif array.size == 2 && array[0] != array[1] # якщо в масиві всього два елементи і вони різні, то один з них в будь-якому випадку локальний максимум
    counter += 1
end

puts counter