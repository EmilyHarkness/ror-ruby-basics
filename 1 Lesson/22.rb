# Дан целочисленный массив. Определить количество участков, на которых его элементы монотонно убывают.

array = [-2, 5, -7, 8, 10, -20, 12]
counter = 0
go_down = false

array.each_cons(2) { |curr, nxt|
    if curr > nxt
        go_down = true
    elsif go_down == true
        go_down = false
        counter += 1
    end
}

if go_down == true
    counter += 1
end

puts counter