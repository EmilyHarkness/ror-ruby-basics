# Дан целочисленный массив. Преобразовать его, прибавив к четным числам последний элемент. Первый и последний элементы массива не изменять.

array = [2, 5, 7, 8, 10, 20]
array = array.map.with_index { |e, i| 
    if e.even? && i != 0 && i != array.size - 1
        e + array.last
    else
        e
    end
}
puts array