# Дан целочисленный массив. Определить количество участков, на которых его элементы монотонно возрастают.

array = [-2, 5, -7, 8, 10, -20, 12]
counter = 0
go_up = false

array.each_cons(2) { |curr, nxt|
    if curr < nxt
        go_up = true
    elsif go_up == true
        go_up = false
        counter += 1
    end
}

if go_up == true
    counter += 1
end

puts counter