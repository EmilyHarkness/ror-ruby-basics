# Дан целочисленный массив. Заменить все отрицательные элементы на значение максимального.

array = [-2, 5, -7, 8, 10, -20]
max_element = array.max

array = array.map { |e| 
    if e < 0
        max_element
    else
        e
    end
}
puts array