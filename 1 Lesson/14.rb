# Дан целочисленный массив. Осуществить циклический сдвиг элементов массива вправо на одну позицию.

array = [-2, 5, -7, 8, 10, -20]
last_element = array.last

array = array.map.with_index { |e, i| 
    if i > 0
        e = array[i - 1]
    else
        last_element
    end
}
puts array.to_s