# Дан целочисленный массив. Преобразовать его, вставив после каждого отрицательного элемента нулевой элемент.

array = [-2, 5, -7, 8, 10, -20, 12]
array = array.map { |e| e < 0 ? [e, array.first] : e }
puts array