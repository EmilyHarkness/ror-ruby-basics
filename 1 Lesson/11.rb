# Дан целочисленный массив. Заменить все отрицательные элементы на значение минимального.

array = [-2, 5, -7, 8, 10, -20]
min_element = array.min

array = array.map { |e| 
    if e < 0
        min_element
    else
        e
    end
}
puts array