# Дан целочисленный массив. Необходимо вывести вначале его элементы с нечетными индексами, а затем - четными.

array = [10, 20, 30, 40, 50]
new_array = array.select.with_index { |e, i| i.odd? } + array.select.with_index { |e, i| i.even? }
puts "Elements with new order: " + new_array.to_s

# Більш швидкодійним варіантом було б, зробити це через два цикли зі степом +=2