require 'yaml'
require 'sinatra'
require 'sinatra/reloader'
require_relative 'pet'

ROOT_PATH = "http://localhost:4567/"

get '/' do
  erb :index, :locals => {:can_create => File.zero?('pets.yml')}
end

get '/create' do
  if File.zero?('pets.yml')
    erb :form, :locals => {:owner_name => '',
                           :pet_name => '',
                           :pet_type => '',
                           :can_create => true}
  else
    redirect '/edit'
  end
end

get '/edit' do
  if !File.zero?('pets.yml')
    pet = YAML.load(File.read('pets.yml'))
    erb :form, :locals => {:owner_name => pet.owner_name,
                           :pet_name => pet.name,
                           :pet_type => pet.type,
                           :can_create => false}
  else
    redirect '/create'
  end
end

get '/delete' do
  File.open('pets.yml', 'w') { |file| file.write('') }
  erb :delete
end

post '/pet' do
  if File.zero?('pets.yml')
    type = params['pet_type'].nil? ? 'cat' : params['pet_type']
    pet = Pet.new(params['pet_name'].strip, type)
    pet.owner_name = params['owner_name'].strip
    File.open('pets.yml', 'w') { |file| file.write(pet.to_yaml) }
  else
    pet = YAML.load(File.read('pets.yml'))
    pet.name = params['pet_name'].strip
    pet.owner_name = params['owner_name'].strip
    File.open('pets.yml', 'w') { |file| file.write(pet.to_yaml) }
  end
  update_page
end

get '/game' do
  update_page
end

get '/game/:action' do
  do_action(params['action'])
end

def update_page
  pet = YAML.load(File.read('pets.yml'))
  erb :pet, :locals => {:owner_name => pet.owner_name,
                        :pet_name => pet.name,
                        :pet_type => pet.type,
                        :food => pet.food,
                        :mood => pet.mood,
                        :sleep => pet.sleep,
                        :cleanliness => pet.cleanliness,
                        :health => pet.health,
                        :life => pet.life,
                        :message => pet.message,
                        :image => pet.image,
                        :game_continue => pet.game_continue,
                        :root_path => ROOT_PATH}
end

def do_action(command)
  pet = YAML.load(File.read('pets.yml'))
  case command.strip.downcase
  when "sleep"
    pet.go_sleep
  when "play"
    pet.play_games
  when "scratch"
    pet.scratch
  when "walk"
    pet.go_walk
  when "pills"
    pet.give_pills
  when "clean"
    pet.clean
  when "eat"
    pet.give_food
  when "watch"
    pet.watch
  else
    p "I don't understand"
  end
  File.open('pets.yml', 'w') { |file| file.write(pet.to_yaml) }
  update_page
end