class Pet
  attr_accessor :owner_name
  attr_accessor :name
  attr_reader :type
  attr_reader :food
  attr_reader :mood
  attr_reader :sleep
  attr_reader :cleanliness
  attr_reader :health
  attr_reader :life
  attr_reader :game_continue
  attr_reader :message
  attr_reader :image

  def initialize(name, type)
    @name = name
    @type = type
    @food = 100
    @mood = 100
    @sleep = 100
    @cleanliness = 100
    @health = 100
    @life = 100
    @message = "Lets play!"
    @game_continue = true
    @image = update_image('')
  end

  def info
    puts "About #{@name}:\nType: #{@type}\nFood: #{@food.to_s}\nMood: #{@mood.to_s}\nCleanliness: #{@cleanliness.to_s}\nSleep: #{@sleep.to_s}\nHealth: #{@health.to_s}\nLife: #{@life.to_s}"
  end

  def go_sleep
    if @sleep > 90
      @message = "I don't want to sleep!"
    else
      @message = "Z-z-z " * 3 + "<br>I woke up"
      @image = update_image("sleep")
      @food = update_param(@food, rand(-20..-10))
      @mood = update_param(@mood, rand(-5..5))
      @sleep = update_param(@sleep, 100)
    end
    step
  end

  def give_food
    if @food > 90
      @image = update_image("enough_food")
      @message = "I don't want to eat!"
    else
      @message = "I'm eating"
      @image = update_image("eat")
      @food = update_param(@food, 100)
      @mood = update_param(@mood, rand(-10..10))
      @sleep = update_param(@sleep, rand(-10..-5))
      @health = update_param(@health, rand(-10..10))
    end
    step
  end

  def give_pills
    @message = "I took the pills"
    @image = update_image("pills")
    @health = update_param(@health, 25)
    step
  end

  def clean
    @message = "I'm clean"
    @image = update_image("clean")
    @cleanliness = update_param(@health, 100)
    @food = update_param(@food, rand(-5..-1))
    @sleep = update_param(@sleep, rand(-5..-1))
    @mood = update_param(@mood, rand(-20..-10))
    step
  end

  def scratch
    case @type
    when "cat"
      @message = "Mur-mur-mur"
    else
      @message = "I like this"
    end

    @image = update_image("scratch")
    @mood = update_param(@mood, 10)
    step
  end

  def play_games
    case @type
    when "cat"
      @message = "Meow-meow-meow"
    when "dog"
      @message = "Uf-uf-uf"
    when "bird"
      @message = "#{@name} good par-rrr-rot"
    else
      @message = "I'm playing"
    end

    @image = update_image("play")
    @food = update_param(@food, rand(-15..-10))
    @mood = update_param(@mood, rand(10..50))
    @sleep = update_param(@sleep, rand(-10..-5))
    step
  end

  def go_walk
    case @type
    when "cat"
      @message = "I see birds! I will catch one for you"
    when "dog"
      if rand(0..1) == 0
        @message = "I'm happy! I'm so happy! Uf-uf!"
      else
        @message = "I'm find something. Om-nom-nom"
        @food = update_param(@food, 10)
        @health = update_param(@health, rand(-10..0))
      end
    when "bird"
      @message = "I see cats! I think it was bad idea. Aaaaaa!"
      @game_continue = false
    when "raccoon"
      @message = "Catch me!"
    else
      @message = "I'm walking"
    end

    @image = update_image("walk")
    @food = update_param(@food, rand(-20..-10))
    @mood = update_param(@mood, rand(20..30))
    @sleep = update_param(@sleep, rand(-10..-5))
    @health = update_param(@health, rand(-10..-1))
    @cleanliness = update_param(@cleanliness, rand(-30..-20))
    step
  end

  def watch
    @food = update_param(@food, rand(-10..-1))
    @mood = update_param(@mood, rand(-15..-5))
    @sleep = update_param(@sleep, rand(-10..-1))

    case rand(0..4)
    when 0
      @image = update_image('')
      @message = "I like to roll on the floor"
      @mood = update_param(@mood, rand(10..20))
      @cleanliness = update_param(@cleanliness, rand(-30..-20))
    when 1
      if @type == "raccoon"
        @image = update_image('raccoon_wash')
        @message = "I found your phone. I have to wash it."
        @mood = update_param(@mood, 20)
      else
        @image = update_image('')
        @message = "I found something. Om-nom-nom"
        @food = update_param(@food, rand(5..20))
        @mood = update_param(@mood, rand(-10..-5))
      end
    else
      @image = update_image('')
      @message = "Nothing happened"
    end

    step
  end

  def help
    puts "You can use this commands:\ninfo - to see information about your pet\nplay - play with your pet in house\nscratch - to scratch your pet\nwalk - walk with your pet outdoors\neat - to eat something\npills - give some pills for your pet\nsleep - go to sleep\nwatch - watching for your pet\nclean - clean your pet\nhelp - to see the list of available commands\nend - finish the game"
  end

  private

  def update_image(status)
    case status
    when "sleep"
      case @type
      when "cat"
        "images/cats/cat-sleep.jpg"
      when "dog"
        "images/dogs/dog-sleep.jpg"
      when "bird"
        "images/birds/bird-sleep.jpg"
      when "raccoon"
        "images/raccoons/raccoon-sleep.jpg"
      else
        "images/cats/cat-sleep.jpg"
      end
    when "play"
      case @type
      when "cat"
        "images/cats/cat-walk.jpg"
      when "dog"
        "images/dogs/dog-walk.jpeg"
      when "bird"
        "images/birds/bird-play.jpg"
      when "raccoon"
        "images/raccoons/raccoon-walk.jpg"
      else
        "images/cats/cat-walk.jpg"
      end
    when "walk"
      case @type
      when "cat"
        "images/cats/cat-walk.jpg"
      when "dog"
        "images/dogs/dog-walk.jpeg"
      when "raccoon"
        "images/raccoons/raccoon-walk.jpg"
      else
        "images/cats/cat-walk.jpg"
      end
      # when "clean"
    when "eat"
      case @type
      when "cat"
        "images/cats/cat-eat.jpg"
      when "dog"
        "images/dogs/dog-eat.jpeg"
      when "bird"
        "images/birds/bird-eat.jpg"
      when "raccoon"
        "images/raccoons/raccoon-eat.jpg"
      else
        "images/cats/cat-eat.jpg"
      end
    when "enough_food"
      case @type
      when "cat"
        "images/cats/cat-enough-food.jpg"
      when "dog"
        "images/dogs/dog.jpg"
      when "bird"
        "images/birds/bird.jpeg"
      when "raccoon"
        "images/raccoons/raccoon.jpg"
      else
        "images/cats/cat-enough-food.jpg"
      end
    when "raccoon_wash"
      "images/raccoons/raccoon-wash.jpg"
    when "game_over"
      case @type
      when "bird"
        "images/birds/bird-game-over.jpg"
      else
        "images/cats/cat-game-over.jpg"
      end
    else
      case @type
      when "cat"
        "images/cats/cat.jpg"
      when "dog"
        "images/dogs/dog.jpg"
      when "bird"
        "images/birds/bird.jpeg"
      when "raccoon"
        "images/raccoons/raccoon.jpg"
      else
        "images/cats/cat.jpg"
      end
    end
  end

  def update_param(param, value)
    param += value
    if param < 0
      0
    elsif param > 100
      100
    else
      param
    end
  end

  def step
    if @game_continue
      if @food < 30 || @sleep < 30
        @health = update_param(@health, -10)
      end

      if 50 <= @mood && @mood < 70
        @message += "<br>I'm bored"
      elsif 10 <= @mood && @mood < 50
        @message += "<br>I'm sad"
      elsif @mood < 10
        @message += "<br>I hate YOU!"
      end

      if 50 <= @sleep && @sleep < 70
        @message += "<br>I'm tired"
      elsif 0 <= @sleep && @sleep < 50
        @message += "<br>I want to sleep"
      end

      if 50 <= @health && @health < 70
        @message += "<br>I'm not feeling well. Maybe you should give me some pills?"
      elsif 30 <= @health && @health < 50
        @message += "<br>I got sick"
      end

      if @cleanliness < 60
        @message += "<br>I got dirty"
      end

      if @health < 30
        @message += "<br>I'm losing my life"
        @life = update_param(@life, -20)
      end

      if 30 <= @food && @food < 65
        @message += "<br>I'm hungry!"
      elsif 0 < @food && @food < 30
        @message += "<br>I'm very very hungry!"
      elsif @food == 0
        @message = "I'm very very hungry! I will eat YOU!"
        @game_continue = false
      end

      if @life == 0
        @game_continue = false
      end
    end

    if !@game_continue
      @message += "<br>GAME OVER!"
      @image = update_image('game_over')
    end

    puts @message
  end
end
